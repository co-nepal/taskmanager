<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('home');

        $menu->addChild('Home', array('route' => 'homepage'));
        /*
        $menu->addChild('About Me', array(
            'route' => 'page_show',
            'routeParameters' => array('id' => 42)
        ));
        */
        // ... add more children
        return $menu;
    }

    public function rightNavbarMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('rightNavbar');

        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $menu->addChild(
                $this->container->get('security.token_storage')->getToken()->getUsername(),
                array('route' => 'homepage')
            );
            $menu->addChild('app.logout', array('route' => 'fos_user_security_logout'))
                ->setExtra('translation_domain', 'app');
        } else {
            $menu->addChild('app.signup', array('route' => 'fos_user_registration_register'))
                ->setExtra('translation_domain', 'app');
        }

        return $menu;
    }
}
