<?php

namespace AppBundle\Propel;

use AppBundle\Propel\om\BaseWorkItem;

class WorkItem extends BaseWorkItem
{
    const TASK = 1;
    const PROBLEM = 2;

    const TODO = 3;
    const CANCELED = 4;
    const DONE = 5;
}
