<?php

namespace AppBundle\Controller;

use Criteria;
use AppBundle\Propel\WorkItem;
use AppBundle\Propel\WorkItemQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $yesterday = date('Y-m-d', strtotime('yesterday'));

        $last_working_day = date('Y-m-d', strtotime('last weekday'.date('now')));

        $tasks = $this->getWikiItemQuery($request->getLocale())
            ->filterByType(WorkItem::TASK)
            ->filterByUpdatedAt(array(
                'min' => $yesterday." 23:59:59",
            ))
            ->orderByUpdatedAt(Criteria::DESC)
            ->find();

        $tasks_pending = $this->getWikiItemQuery($request->getLocale())
            ->filterByType(WorkItem::TASK)
            ->filterByUpdatedAt(array(
                'min' => $last_working_day." 00:00:00",
                'max' => $yesterday." 23:59:59",
            ))
            ->find();

        $problems = $this->getWikiItemQuery($request->getLocale())
            ->filterByType(WorkItem::PROBLEM)
            ->find();

        return array(
            'tasks' => $tasks,
            'tasks_pending' => $tasks_pending,
            'problems' => $problems,
        );
    }

    /**
     * @Route("/search", name="search")
     * @Template()
     */
    public function searchAction(Request $request)
    {
        // TODO implement this
        return $this->redirectToRoute('homepage');
    }

    protected function getWikiItemQuery($locale)
    {
        return WorkItemQuery::create()
            ->filterByUser($this->getUser())
            ->orderByUpdatedAt(Criteria::DESC)
            ->joinWithI18n($locale);
    }
}
