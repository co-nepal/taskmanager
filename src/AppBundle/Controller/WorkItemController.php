<?php

namespace AppBundle\Controller;

use AppBundle\Form\WorkItemType;
use AppBundle\Propel\WorkItem;
use AppBundle\Propel\WorkItemQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/work_item")
 */
class WorkItemController extends Controller
{
    /**
     * @Route("/create", name="work_item_create")
     * @Template()
     */
    public function createTaskAction(Request $request)
    {
        $work_item = new WorkItem();

        $form = $this->createForm(
            new WorkItemType(),
            $work_item,
            array(
                'action' => $this->generateUrl('work_item_create'),
            )
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $work_item->setType(WorkItem::TASK);
            $work_item->setUser($this->getUser());
            $work_item->save();

            return $this->redirectToRoute('homepage');
        }

        return array(
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/{id}/edit", name="work_item_edit")
     * @Template()
     */
    public function editTaskAction(Request $request, $id)
    {
        $work_item = WorkItemQuery::create()
            ->findPk($id);

        // This gives name of column
        //$request->get('name');

        $work_item->setContent($request->get('value'));
        $work_item->save();

        return new Response();
    }

    /**
     * @Route("/create/problem", name="work_item_problem_create")
     * @Template()
     */
    public function createProblemAction(Request $request)
    {
        $work_item = new WorkItem();

        $form = $this->createForm(
            new WorkItemType(),
            $work_item,
            array(
                'action' => $this->generateUrl('work_item_problem_create'),
            )
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $work_item->setType(WorkItem::PROBLEM);
            $work_item->setUser($this->getUser());
            $work_item->save();

            return $this->redirectToRoute('homepage');
        }

        return array(
            'form' => $form->createView(),
        );
    }
}
